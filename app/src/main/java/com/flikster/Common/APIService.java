package com.flikster.Common;

/**
 * Created by sRIHARI on 8/8/2018.
 */


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @POST("registration")
    @FormUrlEncoded
    Call<Post> savePost(@Field("email") String title);
}