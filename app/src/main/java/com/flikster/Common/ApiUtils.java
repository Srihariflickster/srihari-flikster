package com.flikster.Common;

/**
 * Created by Srihari on 8/8/2018.
 */

public class ApiUtils {

    private ApiUtils() {
    }

    public static final String BASE_URL = "http://apiservice.flikster.com/v3/";

    public static final String Register_url =BASE_URL+"user-ms/registration";

    public static final String OTP_url = BASE_URL+"user-ms/checkOtp";

    public static final String ResendOTP_url=BASE_URL+"user-ms/resendOtp";

    public static final String CreateProfile_url = BASE_URL +"user-ms/updateUserById/";

    public static final String Login_url = BASE_URL+"user-ms/login";

    public static final String Feed_url = BASE_URL +"content-ms/contents";

    public static final String GetUser_url = BASE_URL +"user-ms/getUserById/";

    public static final String Forgotpassword_url = BASE_URL+"user-ms/forgotpassword";

    public static final String Changepassword_url = BASE_URL+"user-ms/changePassword";

    public static final String Social_Reg = BASE_URL+"user-ms/socialReg";

    public static final String Checkuser_url = BASE_URL+"user-ms/checkEmail/";

    public static final String Fashion_Url = BASE_URL+"product-ms/products/";

    public static final String GetContent_Url = BASE_URL+"content-ms/getContentByType/";

    public static final String SavedPost_Url =BASE_URL+"user-ms/saveContent/";

    public static final String GetSavedpost_Url = BASE_URL +"user-ms/getSavedPostByUserId/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
