package com.flikster.Common;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

/**
 * Created by Lincoln on 05/05/16.
 */
public class PrefManager {
    SharedPreferences pref,pref_remem;
    SharedPreferences.Editor editor,remeditor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    static final String NAME = "name";
    static final String GUEST_USER_POSTFIX = "_randoms";


    private static final String PREF_Rememberme="remeberme";

    static final String USER_NAME ="username";
    static final String PASSWORD = "password";
    static final String IS_CHESKED_USER ="isuserchecked";



    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        pref_remem = _context.getSharedPreferences(PREF_Rememberme,PRIVATE_MODE);
        remeditor = pref_remem.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public boolean isLoggedIn() {
        String locallySavedUserName = userName();
        return !locallySavedUserName.endsWith(GUEST_USER_POSTFIX);
    }


    public void rememberMe(String username,String password,Boolean isuserchecked){
        remeditor.putString(USER_NAME,username);
        remeditor.putString(PASSWORD,password);
        remeditor.putBoolean(IS_CHESKED_USER,isuserchecked);
        remeditor.commit();
    }

    public void clearrememberme()
    {
        remeditor.clear();
        remeditor.commit();
    }

    public void deletename()
    {
        SharedPreferences.Editor editor=pref.edit();
        editor.remove(NAME);
        editor.commit();
    }

    public void save(String userName) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(NAME, userName);
       // editor.putString(PROFILE_PICTURE, profilePicture);
        editor.commit();
    }

     public void setString(String key,String value)
     {
         SharedPreferences.Editor editor = pref.edit();
         editor.putString(key, value);
         editor.commit();
     }


     public String getString(String key)
     {
         return pref.getString(key,"");
     }


    @NonNull
    public String getUserName()
    {
        return pref_remem.getString(USER_NAME, "");
    }


    @NonNull
    public String getPassword()
    {
        return pref_remem.getString(PASSWORD, "");
    }

    @NonNull
    public Boolean getIsCheskedUser()
    {
        return pref_remem.getBoolean(IS_CHESKED_USER, false);
    }


    @NonNull
    private String userName() {
        return pref.getString(NAME, GUEST_USER_POSTFIX);
    }


}
