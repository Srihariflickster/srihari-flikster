package com.flikster.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity {
  EditText etpassword,etconfirmpassword;
  Button btnchangepass;
  String userid;
  TextView skipandproceed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        etpassword = (EditText) findViewById(R.id.change_etpass);
        etconfirmpassword = (EditText) findViewById(R.id.change_etconfirmpass);
        btnchangepass = (Button) findViewById(R.id.otp_submit);
        skipandproceed = (TextView) findViewById(R.id.changepass_skipproceed);

        Intent intent =getIntent();
        if (intent.hasExtra("userid"))
        {
            userid = getIntent().getExtras().getString("userid");
        }


        skipandproceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnchangepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (etpassword.getText().length()>0 && !etpassword.getText().toString().isEmpty() &&
                        etconfirmpassword.getText().length()>0 && !etconfirmpassword.getText().toString().isEmpty())
                {
                    if (etpassword.getText().toString().equals(etconfirmpassword.getText()
                            .toString())) {
                        Log.v("pasword",etpassword.getText().toString());
                        changepassword(etpassword.getText().toString());
                    } else {
                        Toast.makeText(getBaseContext(),"Passwords are mismatch.",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(),"Please enter values",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void changepassword(final String stpassword) {

         Log.v("userdet",stpassword+","+userid);
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.Changepassword_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject =new JSONObject(response);
                    String statuscode = jsonObject.getString("statusCode");
                    if (statuscode.equals("200"))
                    {
                        Toast.makeText(getBaseContext(),"Password changed Successfully",Toast.LENGTH_SHORT).show();
                        Intent intent =new Intent(getBaseContext(),LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                         Toast.makeText(getBaseContext(),"Failed to change Password",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("id",userid);
                params.put("password",stpassword);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }
}
