package com.flikster.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.Verficationclass;
import com.flikster.R;
import com.flikster.fragments.LoginFrament;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import siclo.com.ezphotopicker.api.EZPhotoPick;
import siclo.com.ezphotopicker.api.EZPhotoPickStorage;
import siclo.com.ezphotopicker.api.models.EZPhotoPickConfig;
import siclo.com.ezphotopicker.api.models.PhotoSource;

public class CreateProfileActivity extends AppCompatActivity implements View.OnClickListener {
  ImageView profilepic;
  Button malebtn,femalebtn; LinearLayout submitbtnlayout;
  EditText usernameedt,password,confirmpass;
    private static final String DEMO_PHOTO_PATH = "MyDemoPhotoDir";
    EZPhotoPickStorage ezPhotoPickStorage;
    Bitmap pickedPhoto;
    String gender;
    String userid,txtemailphone,emailphonetype;
    TextView tvskip;
    Verficationclass verficationclass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_profile);
        verficationclass =new Verficationclass(this);
        init();
        clickevent();
    }

    private void clickevent() {
        tvskip.setOnClickListener(this);
        profilepic.setOnClickListener(this);
        malebtn.setOnClickListener(this);
        femalebtn.setOnClickListener(this);
        submitbtnlayout.setOnClickListener(this);
    }

    private void init() {
        profilepic = (ImageView)findViewById(R.id.profile_imageview);
        malebtn = (Button)findViewById(R.id.profile_male_btn);
        femalebtn = (Button)findViewById(R.id.profile_female_btn);
        submitbtnlayout = (LinearLayout)findViewById(R.id.profile_submitlayout);
        usernameedt = (EditText) findViewById(R.id.profile_username);
        password = (EditText) findViewById(R.id.profile_password);
        confirmpass = (EditText) findViewById(R.id.profile_confirm_pass);
        tvskip  =(TextView) findViewById(R.id.skip_proceed);
         ezPhotoPickStorage = new EZPhotoPickStorage(this);
        Intent intent = getIntent();
        if (intent.hasExtra("userid")) {
            userid = getIntent().getExtras().getString("userid");
        }

        if (intent.hasExtra("txtmobile_email"))
        {
            txtemailphone = getIntent().getExtras().getString("txtmobile_email");
        }

        if (intent.hasExtra("typeemail_phone"))
        {
            emailphonetype = getIntent().getExtras().getString("typeemail_phone");
        }



    }

    @Override
    public void onClick(View v) {
           switch (v.getId())
           {
               case R.id.profile_imageview : Loadimagepic(); break;
               case R.id.profile_male_btn : gender="male";
                                            malebtn.setTextColor(getResources().getColor(R.color.white));
                                            malebtn.setBackgroundResource(R.drawable.shape_male);
                                            femalebtn.setBackgroundResource(R.drawable.shape_female);
                                            femalebtn.setTextColor(getResources().getColor(R.color.gender_color)); break;
               case R.id.profile_female_btn :   gender = "female";
                                                malebtn.setTextColor(getResources().getColor(R.color.gender_color));
                                                malebtn.setBackgroundResource(R.drawable.shape_female);
                                              femalebtn.setBackgroundResource(R.drawable.shape_male);
                                              femalebtn.setTextColor(getResources().getColor(R.color.white)); break;

               case R.id.profile_submitlayout :   if (usernameedt.getText().length()>0 && password.getText().length()>0
                                                       && confirmpass.getText().length()>0)
                                                  {
                                                      if (password.getText().toString().equals(confirmpass.getText()
                                                      .toString()))
                                                      {
                                                      if (gender!=null)
                                                      {
                                                          if (pickedPhoto!=null)
                                                          {
                                                              createprofile();
                                                          } else {
                                                              Toast.makeText(getApplicationContext(),"Please Select Profile Pic",Toast.LENGTH_SHORT).show();
                                                          }
                                                      } else {
                                                          Toast.makeText(getApplicationContext(),"Please Select Gender",Toast.LENGTH_SHORT).show();
                                                      }
                                                  } else {
                                                          Toast.makeText(getApplicationContext(),"Password mismatch.",Toast.LENGTH_SHORT).show();
                                                      }
                                                  }
                                                  else {
                                                      Toast.makeText(getApplicationContext(),"Please enter values",Toast.LENGTH_SHORT).show();
                                                       } break;
               case R.id.skip_proceed :   Intent intent=new Intent(getBaseContext(), MainActivity.class);
                   startActivity(intent); break;
           }
    }

    private void createprofile() {

        verficationclass.showpDialog();
        StringRequest sr = new StringRequest(Request.Method.PUT, ApiUtils.CreateProfile_url+userid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                verficationclass.hidepDialog();
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);
                    String statuscode = jsonObject.getString("statusCode");
                    if (statuscode.equals("200"))
                    {
                        Intent intent=new Intent(getBaseContext(),RegisterandLoginActivity.class);
                        intent.putExtra("fragmenttype","login");
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(),"failed to save details.",Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                //converting image to base64 string
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                pickedPhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                Map<String,String> params = new HashMap<String, String>();
                params.put("pwd",password.getText().toString());
                params.put("username",usernameedt.getText().toString());
                params.put("gender",gender);
                params.put("file",imageString);
                params.put(emailphonetype,txtemailphone);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void Loadimagepic() {

        final AlertDialog builder = new AlertDialog.Builder(CreateProfileActivity.this).create();
        //  final AlertDialog alert = builder.create();
        LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
        View  alertView = inflater.inflate(R.layout.dialog_picimages, null);
        LinearLayout picimagelayout = (LinearLayout)alertView.findViewById(R.id.dialog_piciimagelayout);
        LinearLayout galleryimagelayout = (LinearLayout)alertView.findViewById(R.id.dialog_gallerylayout);


        galleryimagelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                builder.dismiss();
                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.GALLERY;
                config.needToExportThumbnail = true;
                config.isAllowMultipleSelect = true;
                config.storageDir = DEMO_PHOTO_PATH;
                config.exportingThumbSize = 200;
                config.exportingSize = 1000;
                EZPhotoPick.startPhotoPickActivity(CreateProfileActivity.this, config);
            }
        });

        picimagelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
                EZPhotoPickConfig config = new EZPhotoPickConfig();
                config.photoSource = PhotoSource.CAMERA;
                config.storageDir = DEMO_PHOTO_PATH;
                config.needToAddToGallery = true;
                config.exportingSize = 1000;
                EZPhotoPick.startPhotoPickActivity(CreateProfileActivity.this, config);
            }
        });



        builder.setView(alertView);
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }


        if (requestCode == EZPhotoPick.PHOTO_PICK_GALLERY_REQUEST_CODE) {
            try {
                ArrayList<String> pickedPhotoNames = data.getStringArrayListExtra(EZPhotoPick.PICKED_PHOTO_NAMES_KEY);
                showPickedPhotos(DEMO_PHOTO_PATH, pickedPhotoNames);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        if (requestCode == EZPhotoPick.PHOTO_PICK_CAMERA_REQUEST_CODE) {


                try {
                    pickedPhoto = ezPhotoPickStorage.loadLatestStoredPhotoBitmap(300);
                    profilepic.setImageBitmap(pickedPhoto);
                    profilepic.setScaleType(ImageView.ScaleType.CENTER_CROP);
                } catch (IOException e) {
                    e.printStackTrace();
                }

        }
    }
    private void showPickedPhotos(String photoDir, List<String> photoNames) throws IOException {

            for (String photoName : photoNames) {
                 pickedPhoto = ezPhotoPickStorage.loadStoredPhotoBitmap(photoDir, photoName, 300);
                profilepic.setImageBitmap(pickedPhoto);
                profilepic.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
    }







