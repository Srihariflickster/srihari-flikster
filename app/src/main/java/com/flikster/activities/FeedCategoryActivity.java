package com.flikster.activities;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.flikster.R;
import com.flikster.adapters.FeedCategoryAdapter;

public class FeedCategoryActivity extends AppCompatActivity {

    String[] categorynames={"News","First Look","Gallery","Teasers & Promos","Trailers","Posters","Juke Box","Video Songs"
                             ,"Interviews","Movie Making","Comedy Clips","Social Buzz","Dialouges","Tweets","Quotes"};
    String[] categorykeys={"news","first-look","gallery","teasers-promos","trailer","poster","juke-box",
                             "video-song","interview","movie-making","comedy-clip","social-buzz","dialogue",
                              "tweet","quote"};
    RecyclerView catrecycleview;
    FeedCategoryAdapter feedCategoryAdapter;
    LinearLayoutManager layoutManager;
    Button loadcategory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_category);
        catrecycleview = (RecyclerView) findViewById(R.id.categoryrecycle);
        loadcategory = (Button) findViewById(R.id.btn_loadcategory);
        layoutManager=new LinearLayoutManager(getBaseContext());
        catrecycleview.setLayoutManager(layoutManager);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getBaseContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getBaseContext(), R.drawable.divider));
        catrecycleview.addItemDecoration(itemDecorator);
       /* catrecycleview.addItemDecoration(new DividerItemDecoration(getBaseContext(),
                DividerItemDecoration.VERTICAL));*/
        feedCategoryAdapter = new FeedCategoryAdapter(getBaseContext(),categorynames);
        catrecycleview.setAdapter(feedCategoryAdapter);

        loadcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    int selecteditem = feedCategoryAdapter.getmSelectedItem();
                    if (0<=selecteditem) {
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        intent.putExtra("key", categorykeys[selecteditem]);
                        startActivity(intent);
                }
            }
        });


    }
}
