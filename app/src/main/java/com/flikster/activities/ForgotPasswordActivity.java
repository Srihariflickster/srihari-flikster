package com.flikster.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.Verficationclass;
import com.flikster.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity {
EditText et_emailorph;
Button otpsubmit;
Verficationclass verficationclass;
    String emilormobilehint;
    FrameLayout otplayout;
    String message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        et_emailorph = (EditText) findViewById(R.id.forgot_etemailorphone);
        otpsubmit = (Button) findViewById(R.id.otp_submit);
        verficationclass = new Verficationclass(this);
        otplayout = (FrameLayout) findViewById(R.id.forgotframelayout);

        Intent intent = getIntent();
        if (intent.hasExtra("phoneormobile")) {
            emilormobilehint = getIntent().getExtras().getString("phoneormobile");
            et_emailorph.setHint(emilormobilehint);
        }

        otpsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_emailorph.getText().length()>0)
                {
                    if (emilormobilehint.equals("Enter Phone Number")) {
                        if (verficationclass.isValidMobile(et_emailorph.getText().toString().trim())) {
                            sendOtp(et_emailorph.getText().toString().trim(),"mobile");
                               /*Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                               startActivity(intent);*/
                            //  Snackbar.make(otplayout,"Success",Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(otplayout,"Invalid Phone Number..",Snackbar.LENGTH_SHORT).show();
                        }
                    }else {
                        if (verficationclass.isValidMail(et_emailorph.getText().toString().trim())) {
                            sendOtp(et_emailorph.getText().toString().trim(),"email");
                             /*  Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                               startActivity(intent);*/
                            //    Snackbar.make(otplayout,"Success",Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(otplayout,"Invalid Email..",Snackbar.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Snackbar.make(otplayout,"Please enter a value..",Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void sendOtp(final String value, final String email_mobile) {
        // RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.Forgotpassword_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);
                    String code=jsonObject.getString("statusCode");
                    if (jsonObject.has("message")) {
                         message = jsonObject.getString("message");
                    }
                    if (code.equals("200"))
                    {
                        //    Snackbar.make(otplayout,message,Snackbar.LENGTH_SHORT).show();
                        String userid= jsonObject.getString("id");
                        Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                        intent.putExtra("userid",userid);
                        intent.putExtra("activity","Forgot Password");
                        intent.putExtra("emailorphone",email_mobile);
                        intent.putExtra("otptype",value);
                        startActivity(intent);
                    } else if (code.equals("400")) {
                        Snackbar.make(otplayout,message,Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username",value);
                params.put("type",email_mobile);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }


}
