package com.flikster.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flikster.adapters.IndustryAdapter;
import com.flikster.R;

public class IndustryActivity extends AppCompatActivity {
  RecyclerView industryrecyce;
    private LinearLayoutManager layoutManager;
    String[] industry_names={"Bollywood","Tollywood","Tamil","Kannada","Malayalam"};
    Button next,men_fashion,women_fashion;
    SparseBooleanArray sparseBooleanArray;
    private String select="Please select any one Industry";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_industry);
        industryrecyce = (RecyclerView)findViewById(R.id.recycle_industry);
        next = (Button) findViewById(R.id.next_industry);
        men_fashion = (Button) findViewById(R.id.btnmen_fashion);
        women_fashion = (Button) findViewById(R.id.btnwomen_fashin);
        layoutManager = new GridLayoutManager(this,2);
        industryrecyce.setLayoutManager(layoutManager);
        final IndustryAdapter industryAdapter=new IndustryAdapter(getBaseContext(),industry_names);
        industryrecyce.setAdapter(industryAdapter);

        men_fashion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_fashion.setTextColor(getResources().getColor(R.color.white));
                men_fashion.setBackgroundResource(R.drawable.shape_male);
                women_fashion.setBackgroundResource(R.drawable.fashion_buttn);
                women_fashion.setTextColor(getResources().getColor(R.color.block));

            }
        });

        women_fashion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                men_fashion.setTextColor(getResources().getColor(R.color.block));
                men_fashion.setBackgroundResource(R.drawable.fashion_buttn);
                women_fashion.setBackgroundResource(R.drawable.shape_male);
                women_fashion.setTextColor(getResources().getColor(R.color.white));

            }
        });



        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sparseBooleanArray=industryAdapter.getSparseBooleanArray();
                for (int i=0;i<sparseBooleanArray.size();i++) {
                    if (sparseBooleanArray.get(i)==true) {
                        Intent intent = new Intent(getBaseContext(), RegisterandLoginActivity.class);
                        intent.putExtra("fragmenttype", "register");
                        startActivity(intent);
                        select="Successfully select a Industry";
                        break;
                    }
                }
                Toast.makeText(getBaseContext(),select,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
