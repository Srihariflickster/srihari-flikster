package com.flikster.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.flikster.adapters.SlidingImage_Adapter;
import com.flikster.R;

import java.util.ArrayList;

public class IntroductionActivity extends AppCompatActivity {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private static final Integer[] IMAGES= {R.drawable.s_one,R.drawable.s_two,R.drawable.s_three,R.drawable.s_four};

    private ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

    Button nextbutton;
    TextView nexttv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);

        findViews();

        init();

        nexttv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = mPager.getCurrentItem()+1;
                if (current < IMAGES.length) {
                    mPager.setCurrentItem(current);
                } else {
                  //  launchHomeScreen();
                }
            }
        });

        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getBaseContext(),IndustryActivity.class);
                startActivity(intent);
            }
        });





    }

    private void findViews() {
              nextbutton = (Button)findViewById(R.id.btnnext);
              nexttv = (TextView) findViewById(R.id.tvnext);
    }

    private void init() {



        for(int i=0;i<IMAGES.length;i++)
            ImagesArray.add(IMAGES[i]);



        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(IntroductionActivity.this,ImagesArray));
        mPager.addOnPageChangeListener(viewPagerPageChangeListener);

        NUM_PAGES =IMAGES.length;

        // Auto start of viewpager
        /*final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };*/
        /*Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);*/

    }

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == IMAGES.length - 1) {


                nexttv.setVisibility(View.GONE);
                nextbutton.setVisibility(View.VISIBLE);
                //  launchHomeScreen();
                // last page. make button text to GOT IT
                //  btnNext.setText(getString(R.string.start));
                // btnSkip.setVisibility(View.GONE);
            } else {

                nextbutton.setVisibility(View.GONE);
                nexttv.setVisibility(View.VISIBLE);
                // still pages are left
                //  btnNext.setText(getString(R.string.next));
                // btnSkip.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };
}

