package com.flikster.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.PrefManager;
import com.flikster.Common.Verficationclass;
import com.flikster.R;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
EditText etloginmoboremail,etpassword;
TextView tvforgotpass,tvskip;
LinearLayout submitlayout;
String emilormobilehint;
    Verficationclass verficationclass;
    private PrefManager prefManager;
    CheckBox logincheckin;
    Boolean userischecked;
    String message;
    FrameLayout framelayout;

@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(this);
        setContentView(R.layout.activity_login);
        etloginmoboremail = (EditText)findViewById(R.id.etlogin_moboremail);
        etpassword = (EditText) findViewById(R.id.etlogin_pass);
        tvforgotpass = (TextView)findViewById(R.id.login_tvforgot);
        submitlayout = (LinearLayout)findViewById(R.id.login_submitlayout);
        logincheckin = (CheckBox) findViewById(R.id.logincheck);
        framelayout = (FrameLayout) findViewById(R.id.loginframelayout);

        Intent intent =getIntent();

         if (intent.hasExtra("phoneormobile")) {
             emilormobilehint = getIntent().getExtras().getString("phoneormobile");
             etloginmoboremail.setHint(emilormobilehint);
         }

         //
    userischecked = prefManager.getIsCheskedUser();

    if (userischecked== true) {
        etloginmoboremail.setText(prefManager.getUserName());
        etpassword.setText(prefManager.getPassword());
        logincheckin.setChecked(true);
    }



        verficationclass =new Verficationclass(this);

    tvskip = (TextView)findViewById(R.id.skip_proceed);
    tvskip.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }
    });


      logincheckin.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
                  if (!logincheckin.isChecked())
                  {
                      prefManager.clearrememberme();
                  }
          }
      });

        // login
        submitlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etpassword.getText().length()>0 && etloginmoboremail.getText().length()>0)
                {
                    remeberme(etloginmoboremail.getText().toString().trim(), etpassword.getText().toString().trim());
                    if (emilormobilehint.equals("Enter Phone Number")) {
                        if (verficationclass.isValidMobile(etloginmoboremail.getText().toString().trim())) {
                           // remeberme(etloginmoboremail.getText().toString().trim(),etpassword.getText().toString().trim());
                            login(etloginmoboremail.getText().toString().trim(),etpassword.getText().toString().trim());
                        } else {
                            Toast.makeText(getBaseContext(),"Invalid Number",Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        if (verficationclass.isValidMail(etloginmoboremail.getText().toString().trim())) {
                             //  remeberme(etloginmoboremail.getText().toString().trim(), etpassword.getText().toString().trim());
                               login(etloginmoboremail.getText().toString().trim(),etpassword.getText().toString().trim());
                        } else {
                            Toast.makeText(getBaseContext(),"Invalid Email",Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getBaseContext(),"Please Enter the values.",Toast.LENGTH_SHORT).show();
                }

            }
        });



        tvforgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getBaseContext(),ForgotPasswordActivity.class);
                intent.putExtra("phoneormobile",emilormobilehint);
                startActivity(intent);
            }
        });

    }

    private void remeberme(String user, String pass) {
        if (logincheckin.isChecked())
        {
            prefManager.rememberMe(user,pass,true);
        } else {
            prefManager.clearrememberme();
        }

    }

    private void login(final String username, final String password) {
        verficationclass.showpDialog();
        final String devicetoken= FirebaseInstanceId.getInstance().getToken();
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.Login_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject =new JSONObject(response);
                    String status = jsonObject.getString("statusCode");

                    if (jsonObject.has("message")) {
                        message = jsonObject.getString("message");
                    }
                    if (status.equals("200"))
                    {
                        prefManager.save(username);
                        Snackbar.make(framelayout,"Successfully Login",Snackbar.LENGTH_SHORT).show();
                       // Toast.makeText(getApplicationContext(),"Successfully Login",Toast.LENGTH_LONG);
                        JSONObject data =jsonObject.getJSONObject("data");
                        String userid = data.getString("id");
                        prefManager.setString("userid",userid);
                       //  getUserdetails(userid);
                        Intent intent =new Intent(getBaseContext(),MainActivity.class);
                        intent.putExtra("userid",userid);
                        startActivity(intent);
                    } else if (status.equals("400")){
                        Log.v("message",message);
                        Snackbar.make(framelayout,message,Snackbar.LENGTH_SHORT).show();
                      //  Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username",username);
                params.put("password",password);
                params.put("deviceToken",devicetoken);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);



    }

    private void getUserdetails(String userid) {

        StringRequest sr = new StringRequest(Request.Method.GET, ApiUtils.GetUser_url+userid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
              //  verficationclass.hidepDialog();
                JSONObject jsonObject= null;
                try {
                    jsonObject =new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }
            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }
}
