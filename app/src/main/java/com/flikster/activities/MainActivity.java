package com.flikster.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.PrefManager;
import com.flikster.Common.Verficationclass;
import com.flikster.ModelClass.FeedmodelClass;
import com.flikster.R;
import com.flikster.adapters.FeedAdapter;
import com.flikster.fragments.AuctionFragment;
import com.flikster.fragments.FashionFragment;
import com.flikster.fragments.FeedFragment;
import com.flikster.fragments.ProfileFragment;
import com.flikster.fragments.SavedPostFragmnet;
import com.flikster.fragments.WatchFragment;
import com.mikhaellopez.circularimageview.CircularImageView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    RecyclerView recyclerView;
    FeedAdapter feedAdapter;

    ArrayList<FeedmodelClass> arrayList;
    Verficationclass verficationclass;
    LinearLayoutManager layoutManager;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    Toolbar toolBar;
    private static final int FEED_PAGINATION_THRESHOLD = 4;
    ImageView rightnaviamge;
    private View navHeader;
    TextView txtName,txtLocation;
    CircularImageView imgProfile;
    PrefManager prefManager;
    FrameLayout feedframelayout,watchframelayout,fashionframelayout,
                auctinframelayout,profilefrmaelayout;
    Fragment fr ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefManager = new PrefManager(getBaseContext());
        verficationclass = new Verficationclass(this);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        toolBar = (Toolbar)findViewById(R.id.toolbar) ;
        rightnaviamge = (ImageView) findViewById(R.id.navigation_right);
        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtLocation = (TextView) navHeader.findViewById(R.id.website);
        imgProfile = (CircularImageView) navHeader.findViewById(R.id.img_profile);



        // bottom navigation
        feedframelayout = (FrameLayout) findViewById(R.id.feed);
        watchframelayout = (FrameLayout) findViewById(R.id.watch);
        fashionframelayout = (FrameLayout) findViewById(R.id.fashion);
        auctinframelayout = (FrameLayout)findViewById(R.id.Auction_profile);
        profilefrmaelayout = (FrameLayout) findViewById(R.id.profile);
        Intent intent = getIntent();
        if (intent.hasExtra("key")) {
         String key =  getIntent().getExtras().getString("key");
            Bundle bundle = new Bundle();
            bundle.putString("params", key);
            fr = new FeedFragment();
            fr.setArguments(bundle);
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frame, fr);
            fragmentTransaction.commit();
        } else {
            //load default fragment
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.frame, new FeedFragment());
            fragmentTransaction.commit();
        }

        feedframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fr = new FeedFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }
        });
        profilefrmaelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fr = new ProfileFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }
        });
        auctinframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fr = new AuctionFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }
        });

        watchframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fr = new WatchFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }
        });
        fashionframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fr = new FashionFragment();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }
        });

        rightnaviamge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.END);
                }
            }
        });

        navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!prefManager.isLoggedIn()){
                    loadogin();
                }
            }
        });
        // load nav menu header data
        loadNavHeader();
        navigationView.setNavigationItemSelectedListener(this);
   //     getFeeddata();

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        String text = "";
        if (id == R.id.nav_myaccount) {
            text = "My Account";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }

        } else if (id == R.id.nav_orders) {
            text = "Orders";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }

        } else if (id == R.id.nav_wishlist) {
            text = "Wishlist";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }

        } else if (id == R.id.nav_savesposts) {
            text = "Saved posts";
            if (!prefManager.isLoggedIn()){
                loadogin();
            } else {
                fr = new SavedPostFragmnet();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frame, fr);
                fragmentTransaction.commit();
            }

        } else if (id == R.id.nav_refer_friend) {
            text = "Refer a friend";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }

        } else if (id == R.id.nav_credits) {
            text = "Credits";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }
        } else if (id == R.id.nav_aboutus) {
            text = "About us";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }
        } else if (id == R.id.nav_settings) {
            text = "Settings";
            if (!prefManager.isLoggedIn()){
                loadogin();
            }
        } else if (id == R.id.nav_logout) {
            text = "Logout";

            if (!prefManager.isLoggedIn()){
                loadogin();
            } else {
                prefManager.deletename();
            }
        }
        Toast.makeText(this, "You have chosen " + text, Toast.LENGTH_LONG).show();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    //    drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }


    private void loadogin() {
        Intent intent =new Intent(getBaseContext(), RegisterandLoginActivity.class);
        intent.putExtra("fragmenttype","login");
        startActivity(intent);
    }


    private void loadNavHeader() {

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }
}
