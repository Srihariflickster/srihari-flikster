package com.flikster.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.Verficationclass;
import com.flikster.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class OtpCheckActivity extends AppCompatActivity {
TextView tvmob_email,tvnewotp,tvtimer,tvskip;
EditText edt1,edt2,edt3,edt4;
Button btneditnum,btnotpsubmit;
String userid,stemail_phone;
FrameLayout otpframelayout;
Verficationclass verficationclass;
String activity;
TextView otptitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_check);
        verficationclass = new Verficationclass(this);
        tvmob_email = (TextView)findViewById(R.id.otpcheck_tvemailph);
        tvnewotp = (TextView)findViewById(R.id.tvgetnewone);
        edt1 = (EditText)findViewById(R.id.et1);
        edt2 = (EditText)findViewById(R.id.et2);
        edt3 = (EditText)findViewById(R.id.et3);
        edt4 = (EditText)findViewById(R.id.et4);
        otptitle = (TextView) findViewById(R.id.otpname);
        btneditnum = (Button)findViewById(R.id.otpcheck_btnnum);
        btnotpsubmit = (Button)findViewById(R.id.otpcheck_submit);
        otpframelayout =(FrameLayout)findViewById(R.id.otpframe_layout);


        Intent intent =getIntent();

        if (intent.hasExtra("activity"))
        {
            activity = getIntent().getExtras().getString("activity");
            otptitle.setText(activity);
        } else {
            activity = "otp";
        }
        if (intent.hasExtra("userid"))
        {
            userid = getIntent().getExtras().getString("userid");
        }

        if (intent.hasExtra("otptype"))
        {
            tvmob_email.setText(getIntent().getExtras().getString("otptype"));
        }

        if (intent.hasExtra("emailorphone"))
        {
            stemail_phone = getIntent().getExtras().getString("emailorphone");
            if (stemail_phone.equals("mobile")) {
                btneditnum.setVisibility(View.VISIBLE);
            }
        }




        tvskip = (TextView)findViewById(R.id.skip_proceed);
        tvskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        tvnewotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              requestnewotp();

            }
        });

        btnotpsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (edt1.getText().length() ==1 && edt2.getText().length() == 1 && edt3.getText().length() == 1
                       && edt4.getText().length() == 1 )
               {
                   String otpvalue = edt1.getText().toString()+edt2.getText().toString()
                           + edt3.getText().toString() +edt4.getText().toString();
                   Log.v("otpvalue",otpvalue);
                   checkotp(otpvalue);
               } else {
                   Snackbar.make(otpframelayout,"Please enter otp",Snackbar.LENGTH_SHORT).show();
               }
            }
        });
        edt1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Integer textlength1 = edt1.getText().length();

                if (textlength1 >= 1) {
                    edt2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });

        edt2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Integer textlength1 = edt2.getText().length();

                if (textlength1 >= 1) {
                    edt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });

        edt3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Integer textlength1 = edt3.getText().length();

                if (textlength1 >= 1) {
                    edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void requestnewotp() {
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.ResendOTP_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);

                    String statuscode = jsonObject.getString("statusCode");

                    if (statuscode.equals("200"))
                    {
                        Snackbar.make(otpframelayout,"You will get OTP.",Snackbar.LENGTH_SHORT).show();
                    }
                    //String code=jsonObject.getString("statusCode");
                    //  String message = jsonObject.getString("message");
                    /*if (code.equals("200"))
                    {
                        //    Snackbar.make(otplayout,message,Snackbar.LENGTH_SHORT).show();
                        *//*String userid= jsonObject.getString("id");
                        Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                        intent.putExtra("userid",userid);
                        startActivity(intent);*//*
                    } else {
                        Snackbar.make(otpframelayout,"Failed to Check OTP",Snackbar.LENGTH_SHORT).show();
                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("username",tvmob_email.getText().toString());
                params.put("type",stemail_phone);
                params.put("id",userid);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void checkotp(final String otpvalue) {
        verficationclass.showpDialog();
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.OTP_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;
                String mobile_verify = "";
                String email_verify = " ";
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.has("mobileVerify"))
                    {
                         mobile_verify = jsonObject.getString("mobileVerify");
                    }

                    if (jsonObject.has("emailVerify"))
                    {
                         email_verify = jsonObject.getString("emailVerify");
                    }
               //     String mobile_verify = jsonObject.getString("mobileVerify");


                    //  String message = jsonObject.getString("message");

                    if (stemail_phone.equals("mobile"))
                    {
                        if (mobile_verify.equals("yes"))
                        {
                            if (activity.equals("Forgot Password"))
                            {
                                Intent intent = new Intent(getBaseContext(), ChangePasswordActivity.class);
                                intent.putExtra("userid",userid);
                                startActivity(intent);
                            } else if (activity.equals("otp")){
                                Intent intent = new Intent(getBaseContext(), CreateProfileActivity.class);
                                intent.putExtra("userid", userid);
                                intent.putExtra("txtmobile_email", tvmob_email.getText().toString());
                                intent.putExtra("typeemail_phone", stemail_phone);
                                startActivity(intent);
                            }
                        } else {
                            Snackbar.make(otpframelayout,"OTP Verficaton Failed",Snackbar.LENGTH_SHORT).show();
                        }

                    } else if (stemail_phone.equals("email")){
                        if (email_verify.equals("yes"))
                        {
                            if (activity.equals("Forgot Password")) {
                                Intent intent = new Intent(getBaseContext(), ChangePasswordActivity.class);
                                intent.putExtra("userid",userid);
                                startActivity(intent);
                            }else if (activity.equals("otp")){
                                Intent intent = new Intent(getBaseContext(), CreateProfileActivity.class);
                                intent.putExtra("userid", userid);
                                intent.putExtra("txtmobile_email", tvmob_email.getText().toString());
                                intent.putExtra("typeemail_phone", stemail_phone);
                                startActivity(intent);
                            }
                        } else {
                            Snackbar.make(otpframelayout,"OTP Verficaton Failed",Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(otpframelayout,"OTP Verficaton Failed",Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("otp",otpvalue);
                params.put("type",stemail_phone);
                params.put("id",userid);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }
}
