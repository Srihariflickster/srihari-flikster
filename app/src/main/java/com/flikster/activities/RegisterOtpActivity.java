package com.flikster.activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.android.volley.Request;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.flikster.Common.APIService;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;

import com.flikster.Common.Verficationclass;
import com.flikster.R;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.HashMap;
import java.util.Map;



public class RegisterOtpActivity extends AppCompatActivity {
Button submitotp;
EditText etemaioorph;
    int maxLength = 10;
    InputFilter[] fArray = new InputFilter[1];
    Verficationclass verficationclass;
    LinearLayout otplayout;
    private APIService mAPIService;
     String etEmailORPh;
     TextView tvskip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_register);
        submitotp = (Button) findViewById(R.id.otp_submit);
        etemaioorph = (EditText) findViewById(R.id.etemailorphone);
        otplayout = (LinearLayout) findViewById(R.id.otpregisterlayout);

        mAPIService = ApiUtils.getAPIService();

        fArray[0] = new InputFilter.LengthFilter(maxLength);

        verficationclass =new Verficationclass(this);
        Intent intent =getIntent();
        if (intent.hasExtra("emailorphone"))
        {
            etEmailORPh = getIntent().getExtras().getString("emailorphone");
            if(etEmailORPh.equals("Enter Phone Number"))
            {
                etemaioorph.setInputType(InputType.TYPE_CLASS_PHONE);
                etemaioorph.setFilters(fArray);
            }
            etemaioorph.setHint(etEmailORPh);

        }

        tvskip = (TextView)findViewById(R.id.skip_proceed);
        tvskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            }
        });


        submitotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etemaioorph.getText().length()>0)
                {
                       if (etEmailORPh.equals("Enter Phone Number")) {
                           if (verficationclass.isValidMobile(etemaioorph.getText().toString().trim())) {
                               sendOtp(etemaioorph.getText().toString().trim(),"mobile");
                               /*Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                               startActivity(intent);*/
                             //  Snackbar.make(otplayout,"Success",Snackbar.LENGTH_SHORT).show();
                           } else {
                               Snackbar.make(otplayout,"Invalid Phone Number..",Snackbar.LENGTH_SHORT).show();
                           }
                       }else {
                           if (verficationclass.isValidMail(etemaioorph.getText().toString().trim())) {
                               sendOtp(etemaioorph.getText().toString().trim(),"email");
                             /*  Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                               startActivity(intent);*/
                           //    Snackbar.make(otplayout,"Success",Snackbar.LENGTH_SHORT).show();
                           } else {
                               Snackbar.make(otplayout,"Invalid Email..",Snackbar.LENGTH_SHORT).show();
                           }
                       }
                } else {
                    Snackbar.make(otplayout,"Please enter a value..",Snackbar.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void sendOtp(final String value, final String email_mobile) {
       // RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST,ApiUtils.Register_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);
                    String code=jsonObject.getString("statusCode");
                  //  String message = jsonObject.getString("message");
                    if (code.equals("200"))
                    {
                    //    Snackbar.make(otplayout,message,Snackbar.LENGTH_SHORT).show();
                        String userid= jsonObject.getString("id");
                        Intent intent=new Intent(getBaseContext(),OtpCheckActivity.class);
                        intent.putExtra("userid",userid);
                        intent.putExtra("emailorphone",email_mobile);
                        intent.putExtra("otptype",value);
                        startActivity(intent);
                    } else {
                        Snackbar.make(otplayout,"Failed to Register",Snackbar.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(email_mobile,value);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }
}
