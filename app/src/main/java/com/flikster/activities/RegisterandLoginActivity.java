package com.flikster.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.flikster.fragments.LoginFrament;
import com.flikster.fragments.RegisterFragment;
import com.flikster.R;

public class RegisterandLoginActivity extends AppCompatActivity {
   TextView alreadyacont;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Intent intent = getIntent();
        if (intent.hasExtra("fragmenttype"))
        {
            if (getIntent().getExtras().getString("fragmenttype").equals("register"))
            {
                loadFragment(new RegisterFragment());
            } else if (getIntent().getExtras().getString("fragmenttype").equals("login"))
            {
                loadFragment(new LoginFrament());
            }

        }

      /*  alreadyacont = (TextView)findViewById(R.id.alredyacnttv);
        alreadyacont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);
            }
        });*/
    }

    private void loadFragment(Fragment registerFragment) {
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.add(R.id.listcontainer, registerFragment);
        fragmentTransaction.commit(); // save the change
    }
}
