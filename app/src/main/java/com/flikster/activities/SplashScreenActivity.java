package com.flikster.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flikster.Common.PrefManager;
import com.flikster.R;
import com.google.firebase.iid.FirebaseInstanceId;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreenActivity extends AppCompatActivity {
    ImageView gifimageview;
    private PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
       /* if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }*/
        if (Build.VERSION.SDK_INT > 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_splash_screen_acrivity);
        gifimageview = (ImageView)findViewById(R.id.gifimage);
        Glide.with(getBaseContext()).load(R.drawable.splash_log).asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).crossFade().into(gifimageview);
        final String devicetoken= FirebaseInstanceId.getInstance().getToken();
//       Log.v("devicetoken",devicetoken);
     //   getFAcebookkey();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!prefManager.isFirstTimeLaunch())
                {
                    launchHomeScreen();
                } else {
                    prefManager.setFirstTimeLaunch(false);
                    Intent i = new Intent(getBaseContext(), IntroductionActivity.class);
                    startActivity(i);
                    finish();
                }
            }

        }, 3 * 1000);
    }

    private void launchHomeScreen() {
        Intent i = new Intent(getBaseContext(), MainActivity.class);
        startActivity(i);
        finish();
    }
}

