package com.flikster.adapters;

/**
 * Created by Srihari on 8/31/2018.
 */


        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import com.flikster.R;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;

/**
 * Created by Srihari on 7/22/2018.
 */


public class FashionAdapter extends RecyclerView.Adapter<FashionAdapter.MyViewHolder> {

    private static final String TAG = FashionAdapter.class.getSimpleName();
    private Context context;
    ArrayList<Bitmap> images;
    String[] categorynames;
    String totalresponse;

    public FashionAdapter(Context context, String[] fashion_category, String response) {
        this.context = context;

        this.categorynames =new String[fashion_category.length];

        for (int k=0;k<fashion_category.length;k++)
        {
           String categoryname=fashion_category[k];
           this.categorynames[k]=categoryname;

        }

        this.totalresponse = response;

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        View views;
        TextView categoryname;

        public MyViewHolder(View rootView) {
            super(rootView);
            categoryname = (TextView) rootView.findViewById(R.id.categorytv);

            views = rootView;
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fashionlist, parent, false);



        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        Log.v("category","size "+this.categorynames[position]);
        holder.categoryname.setText(this.categorynames[position]);

        try {
            JSONObject jsonObject = new JSONObject(totalresponse);
            JSONArray jsonArray =jsonObject.getJSONArray("Items");
            for (int i=0;i<jsonArray.length();i++)
            {
                if (categorynames[position].equals("Celebrity Store"))
                {

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*holder.itemImage.setImageBitmap(images.get(position));
        holder.itemImage.setScaleType(ImageView.ScaleType.CENTER_CROP);*/
    }


    @Override
    public int getItemCount() {
        return categorynames.length;
    }


}
