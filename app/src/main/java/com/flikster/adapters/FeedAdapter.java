package com.flikster.adapters;

/**
 * Created by Srihari on 8/10/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.PrefManager;
import com.flikster.ModelClass.FeedmodelClass;
import com.flikster.R;
import com.flikster.activities.LoginActivity;
import com.flikster.activities.OtpCheckActivity;
import com.flikster.activities.RegisterandLoginActivity;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Srihari on 7/22/2018.
 */


public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.MyViewHolder> {

    private static final String TAG = FeedAdapter.class.getSimpleName();
    private Context context;
    ArrayList<Bitmap> images;
    String[] names;
    private ArrayList<FeedmodelClass> arrayList;
    PrefManager prefManager;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        private CircularImageView profilepic;
        private ImageView imageView,shareicon,likeicon,bookmarkicon,commenticon;
        private TextView name,proffession,title;
        private Button feedfollow;


        View views;

        public MyViewHolder(View rootView) {
            super(rootView);
            commenticon = (ImageView)rootView.findViewById(R.id.feed_commentsendicon);
            bookmarkicon = (ImageView)rootView.findViewById(R.id.feedbookmark);
            likeicon = (ImageView)rootView.findViewById(R.id.feedlike);
            feedfollow = (Button) rootView.findViewById(R.id.feedfollow);
            shareicon = (ImageView)rootView.findViewById(R.id.feedshare);
            profilepic= (CircularImageView)rootView.findViewById( R.id.feed_profilepic);
            imageView = (ImageView) rootView.findViewById( R.id.feed_posterimage );
            name =(TextView) rootView.findViewById(R.id.profile_name);
            proffession =(TextView) rootView.findViewById(R.id.feed_proffession);
            title =(TextView) rootView.findViewById(R.id.feed_title);
            views = rootView;
        }
    }


    public FeedAdapter(Context context, ArrayList<FeedmodelClass> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
        prefManager =new PrefManager(context);
       // this.images = images;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feedlist, parent, false);



        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.commenticon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefManager.isLoggedIn()){
                    loadogin();
                }
            }
        });

        holder.likeicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefManager.isLoggedIn()){
                    loadogin();
                }
            }
        });

        holder.bookmarkicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefManager.isLoggedIn()){
                    loadogin();
                } else {
                    savecontent(arrayList.get(position).getFeedid());
                }
            }
        });

        holder.feedfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefManager.isLoggedIn()){
                    loadogin();
                }
            }
        });

        holder.shareicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!prefManager.isLoggedIn()){
                    loadogin();
                }
            }
        });

        if (arrayList.get(position).getName()!=null && !arrayList.get(position).getName().isEmpty())
        {
            holder.name.setText(arrayList.get(position).getName());
        } else {
            holder.name.setText("celeb name");
        }

        if (arrayList.get(position).getProffession()!=null && !arrayList.get(position).getProffession().isEmpty())
        {
            holder.proffession.setText("#"+arrayList.get(position).getProffession());
        } else {
            holder.proffession.setText("#proffession");
        }

        if (arrayList.get(position).getTitle()!=null&& !arrayList.get(position).getTitle().isEmpty())
        {
            holder.title.setText(arrayList.get(position).getTitle());
        } else {
            holder.title.setText("title..");
        }


        if (arrayList.get(position).getPoster()!=null)
        {
            Glide.with(context).load(arrayList.get(position).getPoster()).error(R.drawable.ic_launcher)
                    .into(holder.imageView);
        }

        if (arrayList.get(position).getProfilepic()!=null) {
            Glide.with(context).load(arrayList.get(position).getProfilepic()).error(R.drawable.flikster_logo).
                    into(holder.profilepic);
        }


        /*holder.itemImage.setImageBitmap(images.get(position));
        holder.itemImage.setScaleType(ImageView.ScaleType.CENTER_CROP);*/
    }

    private void savecontent(final String feedid) {
        Log.v("response",feedid);
        // RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.SavedPost_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject = new JSONObject(response);
                    String statuscode = jsonObject.getString("statusCode");
                    if (statuscode.equals("200"))
                    {
                        String message = jsonObject.getString("message");
                        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context,"Failed to insert",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("type","bookmark");
                params.put("entityId",feedid);
                params.put("userId",prefManager.getString("userid"));

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);


    }

    private void loadogin() {
        Intent intent =new Intent(context, RegisterandLoginActivity.class);
        intent.putExtra("fragmenttype","login");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


}
