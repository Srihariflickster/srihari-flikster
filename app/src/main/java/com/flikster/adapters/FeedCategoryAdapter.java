package com.flikster.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.flikster.R;

/**
 * Created by Srihari on 9/4/2018.
 */

public class FeedCategoryAdapter extends RecyclerView.Adapter<FeedCategoryAdapter.MyViewHolder> {

    String[] categorynames;
    Context context;
    public int mSelectedItem = -1;

    public FeedCategoryAdapter(Context context, String[] categorynames)
    {
                 this.categorynames = categorynames;
                 this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categorylist, parent, false);


        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.categorytv.setText(categorynames[position]);
        holder.checkBox.setChecked(position == mSelectedItem);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedItem = position;
                notifyItemRangeChanged(0, categorynames.length);
            }
        });
    }

    public String getcategory()
    {
        return categorynames[mSelectedItem];
    }

    public int getmSelectedItem(){
        return mSelectedItem;
    }


    @Override
    public int getItemCount() {
        return categorynames.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
            TextView categorytv;
            CheckBox checkBox;
        public MyViewHolder(View itemView) {
            super(itemView);
            categorytv = (TextView) itemView.findViewById(R.id.categorytv);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);
        }
    }
}
