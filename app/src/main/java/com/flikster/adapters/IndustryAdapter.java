package com.flikster.adapters;

/**
 * Created by sriahri on 7/30/2018.
 */


        import android.content.Context;
        import android.content.res.ColorStateList;
        import android.graphics.Color;
        import android.support.v7.widget.AppCompatRadioButton;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.util.SparseBooleanArray;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.flikster.R;


/**
 * Created by sp on 12/4/17.
 */

public class IndustryAdapter extends RecyclerView.Adapter<IndustryAdapter.MyViewHolder> {

    private static final String TAG = IndustryAdapter.class.getSimpleName();
    private Context context;
    int[] images;
    String[] names;
    int row_index;
    int itemcount=1;
    SparseBooleanArray sparseBooleanArray;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView itemImage;
        private TextView itemTitle;
        private AppCompatRadioButton rb;

        View views;

        public MyViewHolder(View rootView) {
            super(rootView);

            itemTitle = (TextView)rootView.findViewById( R.id.industrytv);
            rb = (AppCompatRadioButton)rootView.findViewById(R.id.radiobutton);

            views = rootView;
        }
    }


    public IndustryAdapter(Context context,String[] names) {
        this.context = context;
        this.names = names;
        this.sparseBooleanArray = new SparseBooleanArray(names.length);
        for (int i=0;i<names.length;i++) {
            this.sparseBooleanArray.append(i,false);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.industrylist, parent, false);


        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked},
                        new int[]{android.R.attr.state_checked}
                },
                new int[]{

                        Color.DKGRAY
                        , Color.rgb (43,153,216),
                }
        );
        holder.rb.setSupportButtonTintList(colorStateList);
        holder.itemTitle.setText(names[position]);



        if(sparseBooleanArray.get(position)==true){

            holder.rb.setChecked(true);
        } else
        {
            holder.rb.setChecked(false);
        }
        holder.rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemcount!=3) {

                    if (sparseBooleanArray.get(position) == false) {
                         itemcount++;
                        Log.v("itemcount",","+itemcount);
                        sparseBooleanArray.append(position, true);
                    } else {
                        itemcount--;
                        sparseBooleanArray.append(position, false);
                    }
                } else {
                    if (sparseBooleanArray.get(position)==true)
                    {
                        itemcount--;
                        Log.v("itemcount",","+itemcount);
                        sparseBooleanArray.append(position, false);
                    } else {
                        Toast.makeText(context,"You can select only maximum any two Industry",Toast.LENGTH_SHORT).show();
                    }
                }
             notifyDataSetChanged();

                // row_index = position;
               // notifyDataSetChanged();

            }
        });
        holder.views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });




    }

    public SparseBooleanArray getSparseBooleanArray()
    {
        return sparseBooleanArray;
    }

    @Override
    public int getItemCount() {
        return names.length;
    }


}
