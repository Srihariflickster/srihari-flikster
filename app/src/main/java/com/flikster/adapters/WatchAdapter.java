package com.flikster.adapters;

/**
 * Created by Srihari on 9/18/2018.
 */
       import android.content.Context;
       import android.support.annotation.NonNull;
       import android.support.v7.widget.LinearLayoutManager;
       import android.support.v7.widget.RecyclerView;
       import android.util.Log;
       import android.view.LayoutInflater;
       import android.view.View;
        import android.view.ViewGroup;
       import android.widget.TextView;

       import com.android.volley.Request;
       import com.android.volley.Response;
       import com.android.volley.VolleyError;
       import com.android.volley.toolbox.StringRequest;
       import com.flikster.Common.ApiUtils;
       import com.flikster.Common.AppController;
       import com.flikster.Common.Verficationclass;
       import com.flikster.ModelClass.FeedmodelClass;
       import com.flikster.R;

       import org.json.JSONArray;
       import org.json.JSONException;
       import org.json.JSONObject;

       import java.util.ArrayList;
       import java.util.HashMap;
       import java.util.Map;

public class WatchAdapter extends RecyclerView.Adapter<WatchAdapter.MyViewHolder> {

    String[] categories;
    Context context;
    ArrayList<FeedmodelClass> arrayList;
    //RecyclerView recyclerView;
    Verficationclass verficationclass;

    public WatchAdapter(Context context) {

    }

    public WatchAdapter(Context context, String[] watchcategory) {
        this.categories= watchcategory;
        this.context = context;
        this.verficationclass =new Verficationclass(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.watchlist, parent, false);



        return new WatchAdapter.MyViewHolder(rootView);
      //  return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
          holder.textView.setText(categories[position]);
        if (categories[position].equals("Juke Box"))
        {
           // verficationclass.showpDialog();
            arrayList =new ArrayList<>(3);
            WatchchildAdapter watchchildAdapter = new WatchchildAdapter(context,arrayList,"jukebox");
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            holder.recyclerView.setAdapter(watchchildAdapter);
            watchchildAdapter.notifyDataSetChanged();
            //  getitems("juke-box");
        } else if (categories[position].equals("Social Buzz/Interviews"))
        {
            getitems(holder,"social-buzz");
        } else if (categories[position].equals("Movies"))
        {
            getitems(holder,"movie-making");
        } else if (categories[position].equals("Trailers & Promos"))
        {
            getitems(holder,"trailer");
        } else if(categories[position].equals("Comedy")) {
              getitems(holder,"comedy-clip");
           //   verficationclass.hidepDialog();
        }
    }


    @Override
    public int getItemCount() {
        return categories.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        RecyclerView recyclerView;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView=(TextView)itemView.findViewById(R.id.watchtv);
            recyclerView = (RecyclerView)itemView.findViewById(R.id.childrecycleview);
        }
    }

    private void getitems(final MyViewHolder holder, final String category) {
        arrayList = new ArrayList<>();
        StringRequest sr = new StringRequest(Request.Method.GET, ApiUtils.GetContent_Url+category, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            //   verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;

                try {
                    jsonObject =new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Items");
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        FeedmodelClass feedmodelClass = new FeedmodelClass();
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);

                        if (jsonObject1.has("profilePic")) {
                            String poster = jsonObject1.getString("profilePic");
                            feedmodelClass.setPoster(poster);
                        } else {
                            feedmodelClass.setPoster("");
                        }

                        if (jsonObject1.has("title")) {
                            String title = jsonObject1.getString("title");
                            feedmodelClass.setTitle(title);
                        } else {
                            feedmodelClass.setTitle("");
                        }
                        arrayList.add(feedmodelClass);
                    }
                    WatchchildAdapter watchchildAdapter = new WatchchildAdapter(context,arrayList,category);
                    holder.recyclerView.setHasFixedSize(true);
                    holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                    holder.recyclerView.setAdapter(watchchildAdapter);
                    watchchildAdapter.notifyDataSetChanged();
                //    watchViewpagerAdapter = new WatchViewpagerAdapter(getContext(),arrayList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
              //  verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }

}
