package com.flikster.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.flikster.ModelClass.FeedmodelClass;
import com.flikster.R;

import java.util.ArrayList;

/**
 * Created by Abhinav on 9/14/2018.
 */

public class WatchViewpagerAdapter extends PagerAdapter {
    private LayoutInflater inflater;
    public Context context;
    public ArrayList<FeedmodelClass> arrayList;

    public WatchViewpagerAdapter(Context context, ArrayList<FeedmodelClass> arrayList) {

        this.context = context;
        inflater = LayoutInflater.from(context);
        this.arrayList = arrayList;
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.watchpagelist, view, false);

      //  assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.trailerimage);
        TextView tv1 = (TextView) imageLayout.findViewById(R.id.tvwatch_title);

        if (arrayList.get(position).getTitle()!=null&& !arrayList.get(position).getTitle().isEmpty())
        {
            tv1.setText(arrayList.get(position).getTitle());
        } else {
            tv1.setText("title..");
        }
        if (arrayList.get(position).getPoster()!=null)
        {
            Glide.with(context).load(arrayList.get(position).getPoster()).error(R.mipmap.ic_launcher)
                    .into(imageView);
        }
       // imageView.setImageResource(IMAGES.get(position));
         view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }



}
