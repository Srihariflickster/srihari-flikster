package com.flikster.adapters;

/**
 * Created by Srihari on 9/18/2018.
 */
       import android.content.Context;
       import android.support.annotation.NonNull;
       import android.support.v7.widget.CardView;
       import android.support.v7.widget.RecyclerView;
       import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
       import android.widget.FrameLayout;
       import android.widget.ImageView;
       import android.widget.RelativeLayout;
       import android.widget.TextView;

       import com.bumptech.glide.Glide;
       import com.flikster.ModelClass.FeedmodelClass;
       import com.flikster.R;

       import java.util.ArrayList;


public class WatchchildAdapter extends RecyclerView.Adapter<WatchchildAdapter.MyViewHolder> {

    String[] categories;
    Context context;
    ArrayList<FeedmodelClass> arrayList;
    String category;

    public WatchchildAdapter(Context context, ArrayList<FeedmodelClass> arrayList, String category) {
        this.context = context;
        this.arrayList = arrayList;
        this.category = category;

    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.watchchildlist, parent, false);

        return new WatchchildAdapter.MyViewHolder(rootView);
        //  return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
                 if (category.equals("jukebox"))
                 {
                     holder.comedyframe.setVisibility(View.GONE);
                     holder.trilersframe.setVisibility(View.GONE);
                     holder.relativemovies.setVisibility(View.GONE);
                     holder.socialframelayout.setVisibility(View.GONE);
                     holder.jukecard.setVisibility(View.VISIBLE);
                 } else {
                     holder.jukecard.setVisibility(View.GONE);
                 }

                 if (category.equals("social-buzz"))
                 {
                     holder.comedyframe.setVisibility(View.GONE);
                     holder.trilersframe.setVisibility(View.GONE);
                     holder.relativemovies.setVisibility(View.GONE);
                     holder.jukecard.setVisibility(View.GONE);
                     holder.socialframelayout.setVisibility(View.VISIBLE);

                     if (arrayList.get(position).getPoster()!=null)
                     {
                         Glide.with(context).load(arrayList.get(position).getPoster()).error(R.mipmap.ic_launcher)
                                 .into(holder.socialimageView);
                     }
                 } else {
                     holder.socialframelayout.setVisibility(View.GONE);
                 }

                 if (category.equals("movie-making"))
                 {
                     holder.jukecard.setVisibility(View.GONE);
                     holder.socialframelayout.setVisibility(View.GONE);
                     holder.trilersframe.setVisibility(View.GONE);
                     holder.comedyframe.setVisibility(View.GONE);
                     holder.relativemovies.setVisibility(View.VISIBLE);
                     if (arrayList.get(position).getPoster()!=null)
                     {
                         Glide.with(context).load(arrayList.get(position).getPoster()).error(R.mipmap.ic_launcher)
                                 .into(holder.movieimageview);
                     }
                 } else {
                     holder.relativemovies.setVisibility(View.GONE);
                 }

                 if (category.equals("trailer")){

                     holder.jukecard.setVisibility(View.GONE);
                     holder.socialframelayout.setVisibility(View.GONE);
                     holder.relativemovies.setVisibility(View.GONE);
                     holder.comedyframe.setVisibility(View.GONE);
                     holder.trilersframe.setVisibility(View.VISIBLE);
                     if (arrayList.get(position).getPoster()!=null)
                     {
                         Glide.with(context).load(arrayList.get(position).getPoster()).error(R.mipmap.ic_launcher)
                                 .into(holder.trilerimageview);
                     }
                 } else {
                     holder.trilersframe.setVisibility(View.GONE);
                 }

                 if (category.equals("comedy-clip")){
                     holder.socialframelayout.setVisibility(View.GONE);
                     holder.jukecard.setVisibility(View.GONE);
                     holder.relativemovies.setVisibility(View.GONE);
                     holder.trilersframe.setVisibility(View.GONE);
                     holder.comedyframe.setVisibility(View.VISIBLE);
                     if (arrayList.get(position).getPoster()!=null)
                     {
                         Glide.with(context).load(arrayList.get(position).getPoster()).error(R.mipmap.ic_launcher)
                                 .into(holder.comedyiamgeview);
                     }
                 } else {
                     holder.comedyframe.setVisibility(View.GONE);
                 }
    }

    @Override
    public int getItemCount()
    {
        if (category.equals("jukebox"))
        {
            return 3;
        } else {
            return arrayList.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        FrameLayout socialframelayout,jukeframe,trilersframe,comedyframe;
        ImageView socialimageView,movieimageview,trilerimageview,comedyiamgeview;
        CardView moviecard,jukecard;
        RelativeLayout relativemovies;
        public MyViewHolder(View itemView) {
            super(itemView);
            jukecard = (CardView) itemView.findViewById(R.id.card_jukebox);
            comedyframe = (FrameLayout) itemView.findViewById(R.id.frame_comedy);
            comedyiamgeview = (ImageView)itemView.findViewById(R.id.comedy_image);
            trilerimageview = (ImageView) itemView.findViewById(R.id.trailer_image);
            trilersframe = (FrameLayout) itemView.findViewById(R.id.frame_trailer);
            relativemovies = (RelativeLayout) itemView.findViewById(R.id.relativelayout_movies);
            movieimageview =(ImageView) itemView.findViewById(R.id.moviethumbnail);
            moviecard = (CardView) itemView.findViewById(R.id.card_view);
            socialframelayout = (FrameLayout) itemView.findViewById(R.id.frame_socialbuzz);
            jukeframe = (FrameLayout) itemView.findViewById(R.id.frame_jukebox);
            socialimageView = (ImageView) itemView.findViewById(R.id.social_image);
          //  textView=(TextView)itemView.findViewById(R.id.watchtv);
        }
    }
}
