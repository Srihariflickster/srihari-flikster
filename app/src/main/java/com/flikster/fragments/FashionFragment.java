package com.flikster.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.Verficationclass;
import com.flikster.R;
import com.flikster.adapters.FashionAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FashionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FashionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FashionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    Verficationclass verficationclass;
    private OnFragmentInteractionListener mListener;
    RecyclerView fashionrecycle;
    String[] fashion_category= {"Celebrity Store","Movie Store","Women Fashion","Men Fashion","Brands"};

    public FashionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FashionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FashionFragment newInstance(String param1, String param2) {
        FashionFragment fragment = new FashionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_fashion, container, false);
        verficationclass = new Verficationclass(getContext());
        fashionrecycle = (RecyclerView) v.findViewById(R.id.fashionrecycleview) ;

        getFashiondata();
        return v;
    }

    private void getFashiondata() {

        verficationclass.showpDialog();

        StringRequest sr = new StringRequest(Request.Method.GET, ApiUtils.Fashion_Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v("response",response);
                verficationclass.hidepDialog();
                JSONObject jsonObject= null;
                fashionrecycle.setHasFixedSize(true);

                FashionAdapter adapter = new FashionAdapter(getContext(), fashion_category,response);

                fashionrecycle.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

                fashionrecycle.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
