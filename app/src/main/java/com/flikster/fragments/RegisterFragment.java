package com.flikster.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.PrefManager;
import com.flikster.R;
import com.flikster.activities.MainActivity;
import com.flikster.activities.RegisterOtpActivity;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener,GoogleApiClient.ConnectionCallbacks{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView alreadyacont;
    FrameLayout facebooklayout,gplusframelayout;
    LoginButton loginButton;
    CallbackManager callbackManager;
    private OnFragmentInteractionListener mListener;
    private AccessToken accessToken;
    private static String FACEBOOK_FIELD_PICTURE = "picture";
    private static String FACEBOOK_FIELD_DATA = "data";
    private static String FACEBOOK_FIELD_URL = "url";
    private PrefManager prefManager;
    private SignInButton btnSignIn;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;

Context context;
    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login_frament, container, false);
        FrameLayout emailframelayout = (FrameLayout) v.findViewById(R.id.email_framelayout);
        FrameLayout phoneframelayout = (FrameLayout) v.findViewById(R.id.phone_framelayout);
        TextView tvskip = (TextView) v.findViewById(R.id.tvrglogin_skip);
        facebooklayout = (FrameLayout) v.findViewById(R.id.facebookframelayout);
        gplusframelayout = (FrameLayout) v.findViewById(R.id.gplusframelayout);
        btnSignIn = (SignInButton) v.findViewById(R.id.btn_sign_in);
         accessToken = AccessToken.getCurrentAccessToken();
        callbackManager = CallbackManager.Factory.create();
        prefManager = new PrefManager(getActivity());

        loginButton = (LoginButton) v.findViewById(R.id.activity_main_btn_login);
        loginButton.setFragment(this);
        loginButton.setReadPermissions(Arrays.asList(new String[]{"email"})); //"user_birthday", "user_hometown"

        context =getActivity();
        //gplus integration
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(),this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        gplusframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signIn();
             //   btnSignIn.performClick();
            }
        });



        //facebook integration
          callFacebookmethod();
        facebooklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (accessToken != null) {
                    getProfileData();
                } else {
                    loginButton.performClick();
                }
            }
        });
        tvskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });
        phoneframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RegisterOtpActivity.class);
                intent.putExtra("emailorphone", "Enter Phone Number");
                startActivity(intent);
            }
        });
        emailframelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), RegisterOtpActivity.class);
                intent.putExtra("emailorphone", "Enter E-mail");
                startActivity(intent);
            }
        });
        alreadyacont = (TextView) v.findViewById(R.id.alredyacnttv);
        alreadyacont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginFrament secFrag = new LoginFrament();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.listcontainer, secFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                //  Intent intent=new Intent(getActivity(), LoginActivity.class);
                //  startActivity(intent);
            }
        });
        // Inflate the layout for this fragment
        return v;
    }

    private void callFacebookmethod() {
       // Log.d("TAG", "User login successfully");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("TAG", "User login successfully");
                getProfileData();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("TAG", "User cancel login");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("TAG", "Problem for login");
            }

        });

    }

    private void getProfileData() {
        try {
            accessToken = AccessToken.getCurrentAccessToken();
         //   rlProfileArea.setVisibility(View.VISIBLE);
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.d("TAG", "Graph Object :" + object);
                            try {
                                String name = object.getString("name");
                                String email = object.getString(("email"));
                             String   url = response.getJSONObject()
                                        .getJSONObject(FACEBOOK_FIELD_PICTURE)
                                        .getJSONObject(FACEBOOK_FIELD_DATA)
                                        .getString(FACEBOOK_FIELD_URL);
                             checkuser(email,name);
                          //   Reguser(name,email);
                                Log.d("TAG", "Name :" + name+",Email :"+ email + url);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,link,birthday,gender,email,picture.type(large)");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void checkuser(final String email, final String name) {
        StringRequest sr = new StringRequest(Request.Method.GET, ApiUtils.Checkuser_url+email, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject =new JSONObject(response);
                    String count =jsonObject.getString("Count");
                    if (Integer.parseInt(count)==1)
                    {
                        prefManager.save(name);
                        Intent intent =new Intent(getActivity(),MainActivity.class);
                        startActivity(intent);
                    } else {
                        Reguser(email,name);
                    }
                    
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void Reguser(final String name, final String email) {

        StringRequest sr = new StringRequest(Request.Method.POST, ApiUtils.Social_Reg, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;
                try {
                    jsonObject =new JSONObject(response);
                    prefManager.save(name);
                    Intent intent =new Intent(getActivity(),MainActivity.class);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("firstname",name);
                params.put("email",email);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        v.getId();
        switch (v.getId())
        {
            case R.id.btn_sign_in: signIn();
        }
    }

    private void signIn() {
        Log.v("signin","signin");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
           // handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
             //       handleSignInResult(googleSignInResult);
                }
            });
        }
    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            Log.e(TAG, "Name: " + personName + ", email: " + email
                    + ", Image: " + personPhotoUrl);
            checkuser(email,personName);
           /* txtName.setText(personName);
            txtEmail.setText(email);
            Glide.with(getApplicationContext()).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfilePic);

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }*/
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
