package com.flikster.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.flikster.Common.ApiUtils;
import com.flikster.Common.AppController;
import com.flikster.Common.Verficationclass;
import com.flikster.ModelClass.FeedmodelClass;
import com.flikster.R;
import com.flikster.activities.MainActivity;
import com.flikster.adapters.WatchAdapter;
import com.flikster.adapters.WatchViewpagerAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WatchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WatchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WatchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView watchrecycleview;
    ViewPager viewPager;
    WatchViewpagerAdapter watchViewpagerAdapter;

    private OnFragmentInteractionListener mListener;
    ArrayList<FeedmodelClass> arrayList;
    Verficationclass verficationclass;
    private ShimmerFrameLayout mShimmerViewContainer;
    String[] watchcategory= {"Juke Box","Social Buzz/Interviews","Movies","Trailers & Promos","Comedy"};
    private int page;
    private Timer timer;

    public WatchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WatchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WatchFragment newInstance(String param1, String param2) {
        WatchFragment fragment = new WatchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        new RemindTask().cancel();
        timer.cancel();
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        verficationclass =new Verficationclass(getActivity());
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_watch, container, false);
        watchrecycleview = (RecyclerView) v.findViewById(R.id.watchrecycle);
        viewPager = (ViewPager) v.findViewById(R.id.watchviewpager);
        mShimmerViewContainer =(ShimmerFrameLayout) v.findViewById(R.id.shimmer_view_container);
        /*watchViewpagerAdapter = new WatchViewpagerAdapter(getContext());
        viewPager.setAdapter(watchViewpagerAdapter);*/
        getTrailers();
        countdowntimer();
        watchrecycleview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        WatchAdapter watchAdapter = new WatchAdapter(getContext(),watchcategory);
        watchrecycleview.setAdapter(watchAdapter);

        return v;
    }

    private void countdowntimer() {

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
               // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                stopanimation();
               // mTextField.setText("done!");
            }
        }.start();

    }

    private void stopanimation() {
        mShimmerViewContainer.stopShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.GONE);
    }

    private void getTrailers() {
        verficationclass.showpDialog();
        arrayList = new ArrayList<>();
        StringRequest sr = new StringRequest(Request.Method.GET, ApiUtils.GetContent_Url+"trailer", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                verficationclass.hidepDialog();
                Log.v("response",response);
                JSONObject jsonObject= null;

                try {
                    jsonObject =new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Items");
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        FeedmodelClass feedmodelClass = new FeedmodelClass();
                        JSONObject jsonObject1=jsonArray.getJSONObject(i);

                        if (jsonObject1.has("profilePic")) {
                            String poster = jsonObject1.getString("profilePic");
                            feedmodelClass.setPoster(poster);
                        } else {
                            feedmodelClass.setPoster("");
                        }

                        if (jsonObject1.has("title")) {
                            String title = jsonObject1.getString("title");
                            feedmodelClass.setTitle(title);
                        } else {
                            feedmodelClass.setTitle("");
                        }
                      arrayList.add(feedmodelClass);
                    }


                    watchViewpagerAdapter = new WatchViewpagerAdapter(getContext(),arrayList);
                    viewPager.setAdapter(watchViewpagerAdapter);
                    pageSwitcher(3);
                  //  stopanimation();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // stopanimation();
                verficationclass.hidepDialog();
                if (error!=null)
                {
                    Log.v("response",error.toString());
                }

            }
        }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);

    }

    public void pageSwitcher(int seconds)
    {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }


    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    Log.v("pagescroll", String.valueOf(page));


                        viewPager.setCurrentItem(page++);
                }
            });
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
