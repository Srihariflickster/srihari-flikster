package com.flikster.pagination;

// FeedBottomThreshold represents the amount of elements at the bottom of the Feed after which
// the next page needs to be loaded

import android.support.v7.widget.LinearLayoutManager;

import com.flikster.adapters.FeedAdapter;


public class FeedBottomThreshold {
    private int threshold;

    public FeedBottomThreshold(int threshold) {
        this.threshold = threshold;
    }

    public boolean hasBeenReached(FeedAdapter feedAdapter, LinearLayoutManager linearLayoutManager) {
        return feedAdapter.getItemCount() - linearLayoutManager.findLastVisibleItemPosition() <= threshold;
    }
}
