package com.flikster.pagination;

import android.support.v7.widget.RecyclerView;
import android.view.View;

// FeedLoaderViewHolder represents the possessor of the view of the progress item at the bottom of
// the feed

public class FeedLoaderViewHolder extends RecyclerView.ViewHolder {
    public FeedLoaderViewHolder(View itemView) {
        super(itemView);
    }

    public void setVisibility(int visibility) {
        itemView.setVisibility(visibility);
    }
}
