package com.flikster.pagination;

public interface FeedPaginationCallback {
    void loadMore();
}
