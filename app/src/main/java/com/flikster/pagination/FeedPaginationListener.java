package com.flikster.pagination;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Response;
import com.flikster.adapters.FeedAdapter;


// FeedPaginationListener intercepts the scrolls in the feed to notify when it needs to load more

public class FeedPaginationListener extends RecyclerView.OnScrollListener {
    private final FeedBottomThreshold feedBottomThreshold;
    private final FeedPaginationCallback feedPaginationCallback;
    private final LinearLayoutManager linearLayoutManager;
    private final FeedAdapter feedAdapter;
    private boolean isLoading;

    public FeedPaginationListener(Response.Listener<String> feedPaginationCallback,
                                  LinearLayoutManager linearLayoutManager, FeedAdapter feedAdapter,
                                  FeedBottomThreshold feedBottomThreshold) {

        this.feedBottomThreshold = feedBottomThreshold;
        this.feedPaginationCallback = (FeedPaginationCallback) feedPaginationCallback;
        this.linearLayoutManager = linearLayoutManager;
        this.feedAdapter = feedAdapter;
        this.isLoading = false;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        boolean hasThresholdBeenReached = feedBottomThreshold.hasBeenReached(feedAdapter, linearLayoutManager);
        if (isScrollingDown(dy) && hasThresholdBeenReached && !isLoading)
            feedPaginationCallback.loadMore();
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    private boolean isScrollingDown(int dy) {
        return dy > 0;
    }
}
